import Vue from 'vue';
import App from './App';

Vue.config.productionTip = false;

App.mpType = 'app';

// 此处为演示Vue.prototype使用，非uView的功能部分
Vue.prototype.vuePrototype = '枣红';

// 引入全局uView
import uView from 'uview-ui';
Vue.use(uView);

// 此处为演示vuex使用，非uView的功能部分
import store from '@/store';

// 引入uView提供的对vuex的简写法文件
let vuexStore = require('@/store/$u.mixin.js');
Vue.mixin(vuexStore);

// 引入uView对小程序分享的mixin封装
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare);

// i18n部分的配置
// 引入语言包，注意路径
import Chinese from '@/common/locales/zh.js';
import English from '@/common/locales/en.js';

// VueI18n
import VueI18n from '@/common/vue-i18n.min.js';
// 表情包组件
// import VEmojiPicker from 'v-emoji-picker';

// Vue.use(VEmojiPicker);

// VueI18n
Vue.use(VueI18n);

let lang = uni.getSystemInfoSync().language.slice(0, 2) //系统语言
const customLang = uni.getStorageSync('language') //用户设置语言
if (lang != 'zh' && lang != 'en') {
	lang = 'zh'
}
// console.log(uni.getSystemInfoSync().platform)
const i18n = new VueI18n({
	// 默认语言
	locale: customLang || lang,
	// 引入语言文件
	messages: {
		'zh': Chinese,
		'en': English,
	}
});
//设置TabBarText
Vue.prototype.$SetTabBarText = t => {
	uni.setTabBarItem({
		index: 0,
		text: t.$t('Tab.home'),
	})
	uni.setTabBarItem({
		index: 1,
		text: t.$t('Tab.message'),
	})
	uni.setTabBarItem({
		index: 2,
		text: t.$t('Tab.me'),
	})
}
//图片转base64
Vue.prototype.$pathToBase64 = (url) => {
	console.log(url)
	return new Promise((reslove, reject) => {
		uni.request({
			url: url,
			method: 'GET',
			responseType: 'arraybuffer',
			success: ress => {
				let base64 = uni.arrayBufferToBase64(ress.data); //把arraybuffer转成base64 
				// console.log(base64)
				base64 = 'data:image/jpeg;base64,' + base64 //不加上这串字符，在页面无法显示的哦
				reslove(base64)
			},
			fail: (e) => {
				reject("图片转换失败");
			}
		})
	})
}
// 时间格式化
Vue.prototype.$FormatDate = (t = Date.now()) => {
	const time = new Date(t)
	const years = time.getFullYear()
	const month = String(time.getMonth() + 1).padStart(2, 0)
	const day = String(time.getDate()).padStart(2, 0)
	const hours = String(time.getHours()).padStart(2, 0)
	const minutes = String(time.getMinutes()).padStart(2, 0)
	const seconds = String(time.getSeconds()).padStart(2, 0)
	return `${years}-${month}-${day} ${hours}:${minutes}:${seconds}`
}
const debounce = (fn,delay) =>{
	let timer = null
	return function(){
		timer && clearTimeout(timer)  
		timer = setTimeout(() => {
			timer = null
			fn()
		},delay)
	}
}
// 时间格式化
Vue.prototype.$WS = () => {
	let {
		email
	} = uni.getStorageSync('userInfo')
	let isSuccessConnect = false
	const connect = () => {
		uni.connectSocket({
			url: 'ws://42.193.131.97:1234?email=' + email,
			success: () => {
				isSuccessConnect = true
			},
			fail: () => {
				isSuccessConnect = false
			},
			complete: () => {
				if (!isSuccessConnect) {
					console.log('ws连接失败，重新连接中...')
					connect()
				}
			}
		});
	}
	connect()
	uni.onSocketOpen(function(res) {
		console.log('WebSocket连接已打开！');
	});
	uni.onSocketError(function(res) {
		debounce(connect,1000)()
		console.log('WebSocket连接打开失败，请检查！');
	});
	uni.onSocketClose(function(res) {
		connect()
		console.log('WebSocket 已关闭！');
	});
	uni.onSocketMessage(res => {
		console.log('收到服务器内容：' + res.data);
		res = JSON.parse(res.data)
		if (res.type == 'chat') {
			uni.showTabBarRedDot({
				index: 1
			})
		}
	});
}
// 由于微信小程序的运行机制问题，需声明如下一行，H5和APP非必填
Vue.prototype._i18n = i18n;

const app = new Vue({
	i18n,
	store,
	...App
});

// http拦截器，将此部分放在new Vue()和app.$mount()之间，才能App.vue中正常使用
import httpInterceptor from '@/common/http.interceptor.js';
Vue.use(httpInterceptor, app);

// http接口API抽离，免于写url或者一些固定的参数
import httpApi from '@/common/http.api.js';
Vue.use(httpApi, app);

app.$mount();
