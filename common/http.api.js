// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
// 登录
const loginUrl = '/login' 
// 注册
const registerUrl = '/register'
// 是否存在该用户
const isExistUserUrl = '/isExistUser'
// 注册验证码
const registerCodeUrl = '/register/code'
// 修改密码验证码
const updatePasswordCodeUrl = '/passwordUpdate/code'
// 修改密码
const updatePasswordUrl = '/passwordUpdate'
// 获取个人信息
const getUserInfoUrl = '/userInfo'
// 修改个人信息
const updateUserInfoUrl = '/userInfo/update'
// 注销账号
const delUserUrl = '/delUser'
// 修改密码通过Token
const updatePasswordByTokenUrl = '/passwordUpdateByToken'
//检查登陆状态
const checkLoginStateUrl = '/checkLoginState'
//新建题目
const saveQuestionUrl = '/saveQuestion'
//获取题目
const getQuestionUrl = '/getQuestion'
//获取试卷名称
const getQuestionNameUrl = '/getQuestionName'
// 获取好友列表
const getUserListsUrl = '/getUserLists'
// 获取课程列表
const getCourseUrl = '/getCourse'
// 提交成绩
const saveScoreUrl = '/saveScore'
// 获取成绩
const getScoreUrl = '/getScore'
// 获取成绩
const getAllScoreByNameUrl = '/getAllScoreByName'
// 保存课程
const saveMyCourseUrl = '/saveMyCourse'
// 获取课程
const getMyCourseUrl = '/getMyCourse'
// 删除课程
const delMyCourseUrl = '/delMyCourse'
// 添加课程视频
const saveMyCourseVideoUrl = '/saveMyCourseVideo'
// 获取课程视频
const getMyCourseVideoUrl = '/getMyCourseVideo'
// 保存完成视频
const saveFinishCourseUrl = '/saveFinishCourse'
const install = (Vue, vm) => {
	// 获取注册验证码
	let getRegisterCode = (params = {}) => vm.$u.get(registerCodeUrl, params);
	// 登录
	const login = (params = {}) => vm.$u.post(loginUrl, params);
	//注册
	const register = (params = {}) => vm.$u.post(registerUrl, params);
	// 查询是否存在该用户
	const isExistUser = (params = {}) => vm.$u.post(isExistUserUrl, params);
	// 获取修改密码验证码
	const getUpdatePasswordCode = (params = {}) => vm.$u.get(updatePasswordCodeUrl, params);
	// 修改密码
	const updatePassword = (params = {}) => vm.$u.post(updatePasswordUrl, params);
	// 获取个人信息
	const getUserInfo = (params = {}) => vm.$u.get(getUserInfoUrl, params);
	// 修改个人信息
	const updateUserInfo = (params = {}) => vm.$u.post(updateUserInfoUrl, params);
	// 注销账号
	const delUser = (params = {}) => vm.$u.post(delUserUrl, params);
	// 修改密码通过Token
	const updatePasswordByToken = (params = {}) => vm.$u.post(updatePasswordByTokenUrl, params);
	// 检查登陆状态
	const checkLoginState = (params = {}) => vm.$u.post(checkLoginStateUrl, params);
	// 保存题目
	const saveQuestion = (params = {}) => vm.$u.post(saveQuestionUrl, params);
	// 获取题目
	const getQuestion = (params = {}) => vm.$u.get(getQuestionUrl, params);
	// 获取试卷名称
	const getQuestionName = (params = {}) => vm.$u.get(getQuestionNameUrl, params);
	// 获取好友列表
	const getUserLists = (params = {}) => vm.$u.get(getUserListsUrl, params);
	// 获取课程列表
	const getCourse = (params = {}) => vm.$u.get(getCourseUrl, params);
	// 保存成绩
	const saveScore = (params = {}) => vm.$u.post(saveScoreUrl, params);
	// 获取成绩
	const getScore = (params = {}) => vm.$u.get(getScoreUrl, params);
	// 获取成绩
	const getAllScoreByName = (params = {}) => vm.$u.get(getAllScoreByNameUrl, params);
	// 保存课程
	const saveMyCourse = (params = {}) => vm.$u.post(saveMyCourseUrl, params);
	// 获取成绩
	const getMyCourse = (params = {}) => vm.$u.get(getMyCourseUrl, params);
	// 删除课程
	const delMyCourse = (params = {}) => vm.$u.post(delMyCourseUrl, params);
	// 添加课程视频
	const saveMyCourseVideo = (params = {}) => vm.$u.post(saveMyCourseVideoUrl, params);
	// 获取课程视频
	const getMyCourseVideo = (params = {}) => vm.$u.get(getMyCourseVideoUrl, params);
	// 保存已看完课程视频
	const saveFinishCourse = (params = {}) => vm.$u.post(saveFinishCourseUrl, params);
	// 保存已看完课程视频百分比
	const saveFinishCourseProgress = (params = {}) => vm.$u.post('/saveFinishCourseProgress', params);
	// 获取观看记录
	const getFinishCourse = (params = {}) => vm.$u.get('/getFinishCourse', params);
	// 获取所有观看记录
	const getAllFinishCourse = (params = {}) => vm.$u.get('/getAllFinishCourse', params);
	// 保存作业完成记录
	const saveFinishHomework = (params = {}) => vm.$u.post('/saveFinishHomework', params);
	// 获取作业是否已完成
	const getIsFinish = (params = {}) => vm.$u.get('/getIsFinish', params);
	const updateFinishHomework = (params = {}) => vm.$u.post('/updateFinishHomework', params);
	// 课程推荐
	const getRecommendCourse = (params = {}) => vm.$u.get('/getRecommendCourse', params);
	// 同类课程推荐
	const getSameTagCourse = (params = {}) => vm.$u.get('/getSameTagCourse', params);
	// 资源获取
	const getAllFile = (params = {}) => vm.$u.get('/getAllFile', params);
	const delFileByID = (params = {}) => vm.$u.post('/delFileByID', params);
	const delQ = (params = {}) => vm.$u.post('/delQ', params);
	const saveSignIn = (params = {}) => vm.$u.post('/saveSignIn', params);
	const getSignIn = (params = {}) => vm.$u.get('/getSignIn', params);
	const postSignIn = (params = {}) => vm.$u.post('/postSignIn', params);
	const delSignIn = (params = {}) => vm.$u.post('/delSignIn', params);
	const getUserBySignIn = (params = {}) => vm.$u.get('/getUserBySignIn', params);
	const getUserByNoSignIn = (params = {}) => vm.$u.get('/getUserByNoSignIn', params);
	const saveNotice = (params = {}) => vm.$u.post('/saveNotice', params);
	const getNotice = (params = {}) => vm.$u.get('/getNotice', params);
	const delNotice = (params = {}) => vm.$u.post('/delNotice', params);
	const saveDiscuss = (params = {}) => vm.$u.post('/saveDiscuss', params);
	const getDiscuss = (params = {}) => vm.$u.get('/getDiscuss', params);
	const delDiscuss = (params = {}) => vm.$u.post('/delDiscuss', params);
	const saveDiscussContent = (params = {}) => vm.$u.post('/saveDiscussContent', params);
	const getDiscussContent = (params = {}) => vm.$u.get('/getDiscussContent', params);
	const delDiscussContent = (params = {}) => vm.$u.post('/delDiscussContent', params);
	const replyDiscussContent = (params = {}) => vm.$u.post('/replyDiscussContent', params);
	vm.$u.api = {
		login,
		isExistUser, 
		getRegisterCode, 
		register, 
		getUpdatePasswordCode,
		updatePassword,
		getUserInfo,
		updateUserInfo,
		delUser,
		updatePasswordByToken,
		checkLoginState,
		saveQuestion,
		getQuestion,
		getQuestionName,
		getUserLists,
		getCourse,
		saveScore,
		getScore,
		getAllScoreByName,
		saveMyCourse,
		getMyCourse,
		delMyCourse,
		saveMyCourseVideo,
		getMyCourseVideo,
		saveFinishCourse,
		getFinishCourse,
		saveFinishCourseProgress,
		getAllFinishCourse,
		saveFinishHomework,
		getIsFinish,
		updateFinishHomework,
		getRecommendCourse,
		getSameTagCourse,
		getAllFile,
		delFileByID,
		delQ,
		saveSignIn,
		getSignIn,
		postSignIn,
		delSignIn,
		getUserBySignIn,
		getUserByNoSignIn,
		saveNotice,
		getNotice,
		delNotice,
		saveDiscuss,
		getDiscuss,
		delDiscuss,
		saveDiscussContent,
		getDiscussContent,
		delDiscussContent,
		replyDiscussContent
		};
}

export default {
	install
}