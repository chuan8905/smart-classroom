export default {
	ok:'Ok',
	cancel:'Cancel',
	confirmChange:'Confirm Change',
	serverError:'Server Error',
	checkNewVersion:'Detection of new versions',
	newUserToRegister:'New User Registration',
	forgetPassword:'Forget the password?',
	login:'Login',
	register:'Register',
	rememberMe:'RememberMe',
	gettingCode:'Getting a CAPTCHA',
	getCode:'Get a CAPTCHA',
	waitTime:'Send it after the countdown is over',
	infoTitle:'Tips',
	Setting: {
		accountSetting:'AccountSetting',
		updatePassword:'UpdatePassword',
		removeCache:'RemoveCache',
		setLanguage:'setChinese',
		help:'Help',
		about:'About',
		connactServerMan:'ConnactServerMan',
		removeCacheSuccess:'Remove Cache Success'
	},
	AccountManage: {
		sf:'Identity',
		avatar:'Avatar',
		name:'Name',
		sex:'Sex',
		description:'Description',
		email:'Email',
		createTime:'CreateTime',
		removeAccount:'RemoveAccount',
		logout:'Logout',
		alertTitle:'Hazardous operation warning!',
		alertContent:'This action will permanently delete all information of the account！！！'
	},
	Tab: {
		home:'Home',
		message:'Message',
		me:'Me',
	},
	BarTitle: {
		setting:'Setting',
		accountManage:'AccountManage',
		userInfoUpdate:'UserInfoUpdate',
		accountPassword:'PasswordUpdate',
		about:'About',
		help:'Help',
		login:'Login',
		register:'Register',
		findPassword:'FindPassword',
		me:'Me',
		message:'Message',
		home:'Home',
		needToFinish:'To be developed',
	},
	User: {
		email:'Email',
		emailTip:'Please enter email email',
		password:'Password',
		rePassword:'RepeatInput',
		passwordTip:' Please enter your password',
		rePasswordTip:'Please repeat your password',
		code:'Captchas',
		codeTip:'Please enter the verification code',
		rules:{
			emailNeed:'Please enter email email',
			emailPattern:'Please enter email correctly',
			noExistEmail:'The email address is not registered',
			existEmail:'The email has been registered',
			passwordNeed:'Please enter your password',
			passwordPattern:'Contains both letters and numbers and should be between 6 and 12 in length',
			rePasswordNeed:'Please repeat your password',
			rePasswordValidator:'The two passwords entered are not equal',
			codeNeed:'Please enter the verification code',
			codePattern:'Captchas can only be numeric',
		}
	},
	Me: {
		needFinish:'NeedFinish',
		score:'Score',
		course:'Course',
		homework:'Homework',
		setting:'Setting',
		noQQ:'You did not install QQ, unable to contact customer service!'
	},
	Home: {
		file:'File System',
		question:'Set a Question',
		live:'Enter Live',
		push:'Living',
		liveRoom:'liveRoom',
		myCourse:'MyCourse',
		myTeachCourse:'MyTeachCourse',
		manageCourse:'ManageCourse',
		progress:'Progress',
		homework:'Homework',
		signIn:'SignIn',
		activity:'Activity',
		notice:'Notice',
		discuss:'Discuss',
		modal:{
			meetingId:'LiveKey',
			meetingIdTip:'Please enter the LiveKey',
			meetingPassword:'Password',
			meetingPasswordTip:'Please fill in the conference password',
			rules:{
				id:'The LiveKey is required',
				id6:'The live number is 6 digits'
			}
		}
	},
	AC:{
	 title:'CourseActivities',
	 signIn:'SignIn',
	 signInStatistics:'SignInStatistics',
	 notice:'Notice',
	 discuss:'Discuss',
	 progressOfTheStatistics:'LearnStatistics'
	},
	Live:{
		liveConfig:'LiveConfig',
		save:'Save',
		level:'Level',
		liveConfigForm:{
			url:"URI",
			urlTip:"format: rtmp://",
			quality:'Quality',
			camera:'Camera',
			cameraDirection:'CameraDirection',
			canFocus:'canFocus',
			focusZoom:'FocusZoom',
			beautyLevel:'Beauty',
			whitenessLevel:'Whiteness',
			mic:'Microphone',
			qualityLevel:{
				sd:'SD',
				hd:'HD',
				fhd:'FHD'
			},
			cameraD:{
				front:'Front',
				back:'Back'
			}
		}
	},
	Msg:{
		helper:'Assistant',
		groupChat:'GroupChat',
		helperText:'Welcome to use wisdom class, you can find me if you have any questions',
		groupChatText:"Let's talk together!",
		helperD1:'Welcome to use wisdom class, you can find me if you have any questions.',
		helperD2:'Hello!I am APP assistant, do you have any questions to ask me!',
	}
}